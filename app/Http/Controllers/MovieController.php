<?php

namespace App\Http\Controllers;

use App\Models\Movie;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;

class MovieController extends Controller
{
    public function __construct()
    {
    }
    public function show($id)
    {
        $movie = Movie::find($id);
        // return  $movie;
        return view('/movie_show')->with('movie', $movie);
    }
    public function store()
    {
    }



    public function index(Request $request)
    {
        $request->flash();
        $movies = Movie::paginate(12);

        if ($request->bytype != '' || $request->bytype != 0) {
            $movies = Movie::title("%$request->bytitle%")
                ->synopsis("%$request->bysynopsis%")
                ->year("%$request->byyear%")
                ->duration("%$request->byduration%")
                ->type("$request->bytype")
                ->get();
        } else {
        }
        return view('/home')->with('movies', $movies);
    }
}
