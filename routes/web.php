<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MovieController;
use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Auth\RegisteredUserController;


use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/',[MovieController::class,'index'])->name('home');


Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__ . '/auth.php';


Route::resource('movies', MovieController::class);
Route::resource('user', UserController::class);

Route::get('/formLogin', function () {
    return view('/auth/formLogin');
});

Route::get('/FAQ', function(){
    return view('FAQ');
});

Route::get('/knowUs', function(){
    return view('knowUs');
});

Route::get('/logout',[AuthenticatedSessionController::class,'destroy'])->name('logout');
Route::get('/login',[AuthenticatedSessionController::class,'create'])->name('login');
Route::get('/register',[RegisteredUserController::class,'create'])->name('register');
Route::get('/index',[MovieController::class,'index'])->name('movies.index');
Route::post('/index',[MovieController::class,'index'])->name('movies.index');


